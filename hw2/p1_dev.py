"""M345SC Homework 2, part 1
Nadya Alexandra Maria Mere // CID: 01202535
"""
import numpy as np

def scheduler(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list my also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    L: A list of integers corresponding to the schedule of tasks. L[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Discussion:
    To complete this function, I created two lists, 'completed' and 'todo', the first of which is a list of zeros
    with the index corresponding to the task. Then go through the list once to complete all the tasks which have no
    prerequisites, and add the incomplete tasks to the 'todo' list. This has O(N).

    Next, while the 'todo' list still has tasks, go through each task and check if the prerequisite tasks have been
    completed. If a prerequisite has not been completed yet, I break the for loop and move on to the next task in
    the to-do list. If all prerequisites have been completed, the task is added to a temporary 'temp' list. At the
    end of the for loop (i.e. at the end of that day), the tasks in 'temp' are removed from the to-do list and marked
    as completed.

    The worst case scenario for this would be if task n required all n-1 tasks before it to be done as well, for
    n=1,...,N-1, with task 0 having no prerequisites i.e. M=N-1. This means it will only complete one task on each
    day. In this worst case, the cost of the method below is approximately C = O(N) + O(NM^2). The best case
    scenario is if M=0, which would have C = O(N).
    """

    N = len(L)
    S= np.zeros(N)
    completed = [0 for n in range(N)]
    todo=[]
    day = 0

    #complete tasks without prerequisites on Day 0 and make to-do list
    for i in range(N):
        if len(L[i])==0:
            completed[i] = 1
        else:
            todo.append(i)

    while len(todo)>0:
        day = day +1
        temp =[]
        for i in todo:
            complete=True
            for m in L[i]:
                if completed[m]==0:
                    complete=False
                    break
            if complete:
                temp.append(i)
                S[i]=day
        #update completed tasks at the end of the day
        for j in temp:
            todo.remove(j)
            completed[j]=1
    return S


def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal
    from node J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2 from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Discussion:
    For this function, used a while loop to go through the nodes and break when it reaches J2
    (or if no feasible path is found). Starting with J1, it explores the adjacent nodes. When it finds a
    path with a>= amin, it breaks the for loop and appends the node to L. The node is then added to the
    dictionary 'explored'. The next iteration moves on to the new node and does the same as previously.
    If an adjacent node has been explored, it skips that node and goes on to the next one.
    If it has no unexplored adjacent nodes with a >= amin, it either breaks the loop and returns an empty list
    or goes back to the previous node and explores another path. It will break the loop if there are no more
    possible paths from J1.

    In the worst case scenario, the cost is C = 0(N) if every node is explored before reaching J2.
    """
    node=J1
    L=[node]
    it = 0
    explored = {node:[]}


    while node != J2:
        it +=1
        adjnodes = A[node]
        feasible = False
        for ind in range(len(adjnodes)):
            if adjnodes[ind][0] in explored.keys():
                continue
            if (adjnodes[ind][1]*a0)>=amin:
                node = adjnodes[ind][0]
                L.append(node)
                feasible=True
                explored[node] = []
                break
        if not feasible:
            if len(L)==1:
                print("findPath: No feasible path from J1 to J2")
                return []
            else: # go back one node and find a different path
                L.pop()
                node = L[-1]

    return L





def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency list, A)

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Discussion:
    Similarly to findPath, the function goes through the nodes until it finds J2. In each iteration,
    a copy of the adjacency list for that node is made, with explored nodes being discarded from the list.
    If all adjacent nodes have been explored, it either goes back to the previous node to explore other nodes,
    or it returns (-1,[]) if it has explored all nodes from J1.
    Otherwise, it makes a list of all the Lij in Acopy and finds the maximum Lij. Finding the maximum costs O(n),
    for n = the number of elements in Acopy, as the function checks every element to find the maximum.
    The node that corresponds to the max(Lij) is appended to L and added to the dictionary, and the value of
    a0min is updated if required.

    At worst, every node is connected to each other, so the length of each sublist is (N-1).
    Hence the cost can be calculated as the sum of an arithmetic sequence, C = O(N^2).
    """

    a0min = -1
    node=J1
    L=[node]
    it = 0
    explored = {node:[]}

    while node != J2:
        Acopy = []
        for ind in range(len(A[node])):
            if A[node][ind][0] not in explored.keys():
                Acopy.append(A[node][ind])
        if len(Acopy)==0:
            if len(L)==1:
                return -1,[]
            else:
                L.pop()
                node = L[-1]
        else:
            Lij = [n[1] for n in Acopy]
            maxL = max(Lij)
            node = Acopy[Lij.index(maxL)][0]
            if a0min*maxL<amin:
               a0min = amin/maxL
            L.append(node)
            explored[node] = []


    return a0min,L

if __name__=='__main__':

    L1 = [[],[0],[0,3,13],[0],[0,1],[0,13],[0,1,4],[],[7],[7,8],[7],[0,7],[0,7,11],[0]]
    S = scheduler(L1)

    #A = [[(1,0.5),(2,0.3),(3,0.1)],[(0,0.5),(4,0.2),(5,0.1)],[(0,0.3),(3,0.2),(7,0.3)],[(0,0.1),(2,0.2)],[(1,0.2),(6,0.5)],[(1,0.1),(6,0.2)],[(4,0.5),(5,0.2),(8,0.1)],[(2,0.3),(8,0.1)],[(6,0.1),(7,0.1)], []]
    A = [[(1,0.2),(2,0.3)],[(0,0.2),(3,0.1)],[(6,0.4),(7,0.1)],[(1,0.1),(4,0.2),(5,0.9)],[(3,0.2),(8,0.1)],[(3,0.3)],[(2,0.4)],[(2,0.1)],[(4,0.1),(9,0.1)],[(8,0.1)]]
    amin = 0.2
    a0 = 5
    J1 = 0
    J2 = 7
    L2 = findPath(A,a0,amin,J1,J2)
    L3 = a0min(A,amin,J1,J2)
