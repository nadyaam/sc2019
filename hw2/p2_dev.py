"""M345SC Homework 2, part 2
Nadya Alexandra Maria Mere // CID: 01202535
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import matplotlib.pyplot as plt

def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    S = np.zeros(Nt+1)

    N = len(G.nodes())
    y0 = np.concatenate((np.zeros(2*N),np.ones(N)))
    y0[x], y0[x+N], y0[x+2*N] = 0.05,0.05,0.1

    def RHS(y,t,a,theta0,theta1,g,k):
        SiVi = (theta0 +theta1*(1-np.sin(2*np.pi*t)))*y[0:N]*y[2*N:3*N]
        dy = np.zeros(3*N)
        dy[0:N] = a*y[N:2*N] - (g+k)*y[0:N]
        dy[N:2*N] = SiVi - (k+a)*y[N:2*N]
        dy[2*N:3*N] = k*(1-y[2*N:3*N])-SiVi
        return dy

    y = odeint(RHS,y0,tarray,args=(a,theta0,theta1,g,k))
    S = y[:,x]

    if display:
        plt.figure(1)
        plt.plot(tarray,S)
        plt.xlabel('Time')
        plt.ylabel('S(t)')
        plt.title('Nadya Mere / model1.py \n S(t) for initial infected node')
    return S

def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):
    """
    Question 2.2
    Simulate model with tau=0.01

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
                each time step.
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    Smean = np.zeros(Nt+1)
    Svar = np.zeros(Nt+1)
    N = len(G.nodes())
    y0 = np.concatenate((np.zeros(2*N),np.ones(N)))
    y0[x], y0[x+N], y0[x+2*N] = 0.05,0.05,0.1
    A = nx.to_numpy_matrix(G)
    qi = np.asarray(G.degree())[:,1]
    F = tau*np.einsum("i,ij->ij",qi,A)/np.einsum("k,kj->j",qi,A)


    def RHS(y,t,a,theta0,theta1,g,k,tau,F):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array conmtaining with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Discussion:
        For each dSi/dt, there are approximately 2N^2+5N operations.
        There are 6 simple arithmetic operations, each applied to N elements. Each of the N rows of the
        matrix multiplication has N multiplications and N-1 sums. Hence the matrix multiplication has
        N(2N-1). Adding them together we get an estimate of 2N^2+5N operations.

        """
        dy = np.zeros(3*N)
        SiVi = (theta0 +theta1*(1-np.sin(2*np.pi*t)))*y[0:N]*y[2*N:3*N]

        dy[0:N] = a*y[N:2*N] - (g+k)*y[0:N] + F @ y[0:N] -  tau*y[0:N]
        dy[N:2*N] = SiVi - (k+a)*y[N:2*N] + F @ y[N:2*N] - tau*y[N:2*N]
        dy[2*N:3*N] = k*(1-y[2*N:3*N])-SiVi + F @ y[2*N:3*N] - tau*y[2*N:3*N]
        return dy

    t = 0
    Smean[0] = np.mean(y0[0:N])
    Svar[0] = np.var(y0[0:N])
    for ind,newt in enumerate(tarray[1:], start=1):
        y = odeint(RHS,y0,[t,newt],args=(a,theta0,theta1,g,k,tau,F))
        y0 = y[1,:]
        t = newt
        Smean[ind] = np.mean(y0[0:N])
        Svar[ind] = np.var(y0[0:N])


    if display:
        plt.figure(2)
        plt.plot(tarray,Smean)
        plt.xlabel('Time')
        plt.ylabel('Mean of S')
        plt.title('Nadya Mere / modelN.py \n Mean of S across nodes')

        plt.figure(3)
        plt.plot(tarray,Svar)
        plt.xlabel('Time')
        plt.ylabel('Variance of S')
        plt.title('Nadya Mere / modelN.py \n Variance of S across nodes')

    return Smean,Svar


def diffusion(input=(None),tau=0.1,x=0,tf=6,Nt=400):
    """Analyze similarities and differences
    between simplified infection model and linear diffusion on
    Barabasi-Albert networks.
    Modify input and output as needed.

    Discussion: add discussion here
    """
    A = nx.adjacency_matrix(G)
    Q = A.toarray().sum(axis=1)
    L = tau*(np.diag(Q)-A.toarray())
    tarray = np.linspace(0,tf,Nt+1)
    N = len(G.nodes())
    y0 = np.concatenate((np.zeros(2*N),np.ones(N)))
    y0[x], y0[x+N], y0[x+2*N] = 0.05,0.05,0.1
    Smean = np.zeros(Nt+1)
    Svar = np.zeros(Nt+1)

    def RHS(y,t,L):
        dy = np.zeros(3*N)
        dy[0:N] = -L@y[0:N]
        dy[N:2*N] = -L@y[N:2*N]
        dy[2*N:3*N] = -L@y[2*N:3*N]
        return dy


    y = odeint(RHS,y0,tarray,args=(L,))
    Smean1 = np.mean(y[:,0:N],axis=1)
    Svar1 = np.var(y[:,0:N],axis=1)

    Smean2, Svar2 = modelN(G,x,params=(0,80,0,0,0,tau),tf=tf,Nt=Nt)

    plt.figure(4)
    plt.plot(tarray,Smean1, label = 'Using infection model in modelN')
    plt.plot(tarray,Smean2, label = 'Using linear diffusion')
    plt.legend()
    plt.xlabel('Time')
    plt.ylabel('Mean of S')
    plt.title('Nadya Mere / diffusion.py \n Mean of S across nodes for the two models')

    plt.figure(5)
    plt.plot(tarray,Svar1, label = 'Using infection model in modelN')
    plt.plot(tarray,Svar2, label = 'Using linear diffusion')
    plt.legend()
    plt.xlabel('Time')
    plt.ylabel('Variance of S')
    plt.title('Nadya Mere / diffusion.py \n Variance of S across nodes for the two models')

    return Smean,Svar


if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    G=nx.barabasi_albert_graph(100,5)
    S1 = model1(G,display=True)
    S2 = modelN(G,display=True)
    S3 = diffusion(G)
    #S4 = modelN(G,params=(0,80,0,0,0,0.01),display=True)
