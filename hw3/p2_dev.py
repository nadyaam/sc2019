"""M345SC Homework 3, part 2
Nadya Alexandra Maria Mere // CID: 01202535
"""
import numpy as np
import networkx as nx
import scipy.linalg

def growth1(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.1
    Find maximum possible growth, G=e(t=T)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion: Add discussion here
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)
    x = 0
    y[2*N:]=np.ones(N)
    y[x], y[x+N], y[x+2*N] = 0.05,0.05,0.1
    #find solution: dy/dt=Ay+Fy
    Nzeros = np.zeros((N,N))
    A = np.block([[-(g+k)*np.eye(N), a*np.eye(N), Nzeros],
                 [theta*np.eye(N), -(k+a)*np.eye(N), Nzeros],
                 [-theta*np.eye(N),Nzeros,k*np.eye(N)]])
    B = np.block([[F, Nzeros, Nzeros],[Nzeros,F, Nzeros],[Nzeros, Nzeros,F]]) - tau*np.eye(3*N)
    yf = scipy.linalg.expm((A+B)*T)@y

    G = (sum(np.power(yf[:N],2) + np.power(yf[N:2*N],2) +
        np.power(yf[2*N:],2)))/(sum(np.power(y[:N],2) + np.power(y[N:2*N],2)
        + np.power(y[2*N:],2)))

    return G,y

def growth2(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.2
    Find maximum possible growth, G=sum(Ii^2)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion: Add discussion here
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)
    x = 0
    y[2*N:]=np.ones(N)
    y[x], y[x+N], y[x+2*N] = 0.05,0.05,0.1
    #find solution: dy/dt=Ay+Fy
    Nzeros = np.zeros((N,N))
    A = np.block([[-(g+k)*np.eye(N), a*np.eye(N), Nzeros],
                 [theta*np.eye(N), -(k+a)*np.eye(N), Nzeros],
                 [-theta*np.eye(N),Nzeros,k*np.eye(N)]])
    B = np.block([[F, Nzeros, Nzeros],[Nzeros,F, Nzeros],[Nzeros, Nzeros,F]]) - tau*np.eye(3*N)
    yf = scipy.linalg.expm((A+B)*T)@y

    G = sum(np.power(yf[N:2*N],2))/(sum(np.power(y[:N],2) + np.power(y[N:2*N],2)
        + np.power(y[2*N:],2)))

    return G,y


def growth3(G,params=(2,2.8,1,1.0,0.5),T=6):
    """
    Question 2.3
    Find maximum possible growth, G=sum(Si Vi)/e(t=0)
    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth

    Discussion: Add discussion here
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)
    x = 0
    y[2*N:]=np.ones(N)
    y[x], y[x+N], y[x+2*N] = 0.05,0.05,0.1
    #find solution: dy/dt=Ay+Fy
    Nzeros = np.zeros((N,N))
    Nones = np.eye(N)
    A = np.block([[-(g+k)*Nones, a*Nones, Nzeros],
                 [theta*Nones, -(k+a)*Nones, Nzeros],
                 [-theta*Nones,Nzeros,k*Nones]])
    B = np.block([[F, Nzeros, Nzeros],[Nzeros,F, Nzeros],[Nzeros, Nzeros,F]]) - tau*np.eye(3*N)
    yf = scipy.linalg.expm((A+B)*T)@y

    G = np.dot(yf[:N],yf[2*N:])/(sum(np.power(y[:N],2) + np.power(y[N:2*N],2)
        + np.power(y[2*N:],2)))

    return G

import ipdb
def Inew(D):
    """
    Question 2.4

    Input:
    D: N x M array, each column contains I for an N-node network

    Output:
    I: N-element array, approximation to D containing "large-variance"
    behavior

    Discussion: Add discussion here
    """
    N,M = D.shape
    # I = np.zeros(N)

    A = (D - np.outer(np.ones((N,1)),D.mean(axis=0))).T
    C = A@A.T
    print("Total variance=",np.trace(C)/N)
    U, S, VT = np.linalg.svd(A)
    G = U.T@A
    I = G[0,:]
    print(S)
    #ipdb.set_trace()
    print("Approximate of total variance=", np.var(I))

    return I


if __name__=='__main__':
    #G=None
    #add/modify code here if/as desired
    N,M = 100,5
    G = nx.barabasi_albert_graph(N,M,seed=1)
    G1,y = growth1(G)
    G2,y = growth2(G)
    G3 = growth3(G)
    D = np.loadtxt("q22test.txt")
    I = Inew(D)
