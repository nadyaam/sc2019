"""M345SC Homework 3, part 1
Nadya Alexandra Maria Mere // CID: 01202535
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.signal import hann
import scipy.sparse
from scipy.linalg import solve_banded

def nwave(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        """
        g = f[:Nx]+1j*f[Nx:]
        #add code here
        c = np.fft.fft(g)/Nx
        n = np.fft.fftshift(np.arange(-Nx/2,Nx/2))
        k = 2*np.pi*n/100
        d2g= Nx*np.fft.ifft(-1*k**2*c)
        #-----------
        dgdt = alpha*d2g + g -beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    f0=np.zeros(2*Nx)
    f0[:Nx]=g0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')

    return g

def analyze():
    """
    Question 1.2
    Add input/output as needed

    Discussion:
    
    """

    alpha = 1-2j
    beta = 1+2j
    g1 = nwave(alpha,beta)
    alpha = 1-1j
    beta = 1+2j
    g2 = nwave(alpha,beta)

    def freqanalysis(T,g1,g2,fig,Nx=256):
        nt = T*4
        c1 = np.fft.fft(g1[nt,:])/Nx
        c2 = np.fft.fft(g2[nt,:])/Nx
        n = np.arange(-Nx/2,Nx/2)
        plt.figure(fig)
        plt.semilogy(n/T,np.abs(np.fft.fftshift(c1))**2, 'x', label="Case A")
        plt.semilogy(n/T,np.abs(np.fft.fftshift(c2))**2,'x', label="Case B")
        plt.xlabel("Frequency, n/T")
        plt.ylabel("$|c|^2$")
        plt.legend()
        plt.title('Nadya Mere / analyze() \n Energy against Frequency for T =%i'%T)
        return None
    freqanalysis(50,g1,g2,1)
    freqanalysis(100,g1,g2,2)
    freqanalysis(150,g1,g2,3)
    freqanalysis(200,g1,g2,4)


    return None #modify as needed

import time
import ipdb
def wavediff(g,Nx=256):
    """
    Question 1.3
    Add input/output as needed

    Discussion: Add discussion here
    """
    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    h = x[1]-x[0]
    x = x[:-1]

    al = 3/8
    a = 25/16/(2*h)
    b = 1/5/(4*h)
    c = -1/80/(6*h)

    #boundary conditions at x=0,L-h
    albound = 3
    abound = -17/6/h
    bbound = 3/2/h
    cbound = 3/2/h
    dbound = -1/6/h

    ones = np.ones(Nx)
    aldiag = al*ones[2:]
    adiagtop,adiagbot = a*ones[1:],-a*ones[1:]
    adiagtop[0], adiagbot[-1] = bbound, -bbound
    bdiagtop, bdiagbot = b*ones[2:], -b*ones[2:]
    bdiagtop[0], bdiagbot[-1] = cbound, -cbound
    cdiagtop, cdiagbot = c*ones[3:], -c*ones[3:]
    cdiagtop[0],cdiagbot[-1] = dbound,-dbound
    zerodiag = np.zeros(Nx)
    zerodiag[0],zerodiag[-1] = abound,-abound

    t1 = time.time()
    Ab = np.block([[0,albound,aldiag],[ones],[aldiag,albound,0]])
    B = scipy.sparse.diags([[b,0],[c,c,0],cdiagbot,bdiagbot,adiagbot,zerodiag,adiagtop,
        bdiagtop,cdiagtop,[0,-c,-c],[0,-b]],[-Nx+2,-Nx+3,-3,-2,-1,0,1,2,3,Nx-3,Nx-2])
    b = B@g
    #ipdb.set_trace()
    dgfd = solve_banded((1,1),Ab,b)
    t2 = time.time()
    print("Time taken with Finite Difference:", t2-t1)
    t3 = time.time()
    n = np.fft.fftshift(np.arange(-Nx/2,Nx/2))
    k = 2*np.pi*n/L
    c = np.fft.fft(g)/Nx
    dgft= Nx*np.fft.ifft(1j*k*c)
    t4 = time.time()
    print("Time taken with DFT:", t4-t3)
    plt.figure(5)
    plt.plot(x,dgfd.real,linewidth=2,label="Finite Difference method")
    plt.plot(x,dgft.real, linewidth=1,label="Discrete Fourier Transform method")
    plt.legend()


    return diff

if __name__=='__main__':
    #x=None
    #alpha = 1-2j
    #beta = 1+2j
    #g1 = nwave(alpha,beta,display=True)
    alpha = 1-1j
    beta = 1+2j
    g2 = nwave(alpha,beta)
    analyze()
    g = wavediff(g2[400,:])
