"""M345SC Homework 1, part 2
Nadya Mere 01202535
"""

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """

    Lout=[]
    N = len(L)
    M = len(L[0])

    #search through each sublist
    for i in range(N):
        #search target in first P sorted elements
        start = 0
        end = P-1

        while start<=end:
            midpoint = int(0.5*(start+end))

            if target==L[i][midpoint]:
                Lout.append([i,midpoint])
                #check for repeated target behind midpoint
                for j in range(1,midpoint+1):
                    if L[i][midpoint-j] ==target:
                        Lout.append([i,midpoint-j])
                    else:
                        break
                #check for repeated target in front of midpoint
                for j in range(midpoint+1,P):
                    if L[i][j] == target:
                        Lout.append([i,j])
                    else:
                        break
                break
            elif target<L[i][midpoint]:
                end = midpoint-1
            else:
                start = midpoint+1

        #search target in last M-P elements
        for j in range(P,M):
            if target == L[i][j]:
                Lout.append([i,j])

    return Lout




#import for test functions
import random
import numpy as np
import time
import matplotlib.pyplot as plt

def Lgenerator(N,M,P):
    L = []
    for i in range(N):
        sublist = []
        for j in range(P):
            sublist.append(random.randint(-100,100))
        sublist = np.ndarray.tolist(np.sort(sublist))
        for j in range(M-P):
            sublist.append(random.randint(-100,100))
        L.append(sublist)

    return L

def nsearch_time(points, Pfactor):
    """Analyze the running time of nsearch.

    Discussion:
    The search algorithm in nsearch uses a for loop to go through each sublist and performs both a
    binary search and a linear search at each iteration. As the first P elements of each sublist are
    sorted, I can make use of a binary search to discard half of the list at each iteration. However, because the numbers
    can be repeated, if the median of the list is found, I've coded in a check for values to its left
    and right. Once the numbers no longer match the target, it breaks the while loop. This part of the
    operation has its worst case scenario if all first P elements are the target. It would then have a
    running time of c1(P) + c2, for some constants ci. Otherwise, it would have a running time of
    c3log(2)P + c4.

    For the second unsorted portion, I've done a linear search through the final (M-P) elements of each
    sublist. This has a worst case scenario running time of 3(M-P) with a maximum of 3 operations per iteration.
    If P is small relative to M, then this linear search will have a large running time.

    Hence running time for nsearch is
                            C = a1(NM) + a2(NP) + a3(Nlog2(P)) + a4(N) + a5
    for some constants ai, with leading order O(NM).
    The best case scenario is if the target is found immediately in the binary search with no repeated terms,
    and the target is not in the final M-P elements of the subset. This would give us a running time of
    2(N(M-P)) + b2N + b3, for some constants bi.

    Thus we see the leading order is dependent on the total number of elements in L, N x M, and it's caused by
    the linear search on the unsorted portion of the sublist as it has O(M-P). Hence, if P is a large proportion
    of M, the function should run faster than it would if P were a small proportion of M. This is because
    it would do the more efficient binary search on a larger proportion of M, thus leaving fewer elements to do a
    linear search on.

    The function can be considered relatively efficient as it utilises the fact that a part of the list is already
    sorted, hence reducing the running time from O(NP) to O(N(log2(P)+1)). However, the unsorted portion of
    the list does slow down the algorithm as P gets smaller, and there is no simple way of making it more efficient.
    A possible way of making the function faster would be to run the two search functions in parallel as they don't
    depend on each other.

    To test nsearch, I coded Lgenerator to generate the list L given N, M, and P, to be used in nsearch_time.
    For nsearch_time, I have the number of iterations (points) and the factor to calculate P based on M (Pfactor)
    as its parameters. In the function, I randomise N and M within a boundary.

    fig1.png: P = M/2 with 20 iterations.
    Even with only 20 points, a rough linear increasing trend can be seen in this graph. I've chosen to do a scatter
    plot as the values do vary a bit and anomalies can be seen.

    fig2.png: P = M/2 with 100 iterations.
    To further explore the trend, I've called nsearch_time for the same value of Pfactor, but for 100 iterations.
    The linear relationship is much clearer here as the plots overlap much more, although there running time begins
    to vary more as (N x M) increases.

    fig3.png: P = M/4 with 100 iterations.
    I've also plotted another graph for P = M/4 with 100 iterations to compare it to the previous graph.
    It is clear that the maximum running time for P = M/4 is longer than that for P = M/2, in fact it's nearly twice as
    long in this figure. The same linearly increasing trend can be seen here, with the same variation in running times
    as (N x M) gets larger.

    fig4.png: Comparison of the scatter plot for varying Pfactor = 3/4,1/2,1/5,1/10.
    I decided to directly compare the plots for various values of P on the same graph. This figure shows that as
    P decreases, so does the gradient of the trend. That is, as P becomes a smaller proportion of M, the increasing
    linear trend becomes more steep. All the graphs start at roughly the same point near the origin, but immediately
    diverge, except for the points with P = M/10, M/5. The points for these values of P overlap each other and follow a
    similar trend, but with more variations in the points for P= M/10. Similarly to before, the linear relationship is
    followed much more closely by the points when (N x M) is smaller, and the points scatter more as the number of
    elements in L increases.
    """

    NM =[]
    dt =[]
    for i in range(points):
        #randomise N,M
        N = random.randint(100,10000)
        M = random.randint(100,1000)
        P = int(M*Pfactor)
        target = random.randint(-101,101)

        L = Lgenerator(N,M,P)
        t1 = time.time()
        Lout = nsearch(L,P,target)
        t2 = time.time()

        dt.append(t2-t1)
        NM.append(N*M)

    dt = np.asarray(dt)
    NM = np.asarray(NM)

    #plot the points
    fig = plt.plot(NM, dt, 'x', label = f'P ={Pfactor}M')
    plt.ylabel('Time taken to execute nsearch')
    plt.xlabel('Number of sublists in L x Number of elements in a sublist')
    plt.title('Nadya Mere / nsearch_time() \n Running time against the number of elements in L for varying P')
    plt.legend()
    return fig


if __name__ == '__main__':

    A = nsearch_time(100,3/4)
    B = nsearch_time(100,1/2)
    C = nsearch_time(100,1/5)
    D = nsearch_time(100,1/10)
