"""M345SC Homework 1, part 1
Nadya Mere / 01202535
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion:
    This function utilises dictionaries to store each k-mer in S as a key with its
    number of occurencences in S assigned as its value. The use of dictionary comprehension here
    has a running time of (n-k+1).

    To create L1, a for loop goes through the dictionary and appends the keys with a value pair
    that is greater than or equal to f. As dictionaries are hash tables, if a key is repeated, the previous
    key is deleted and the new one is stored. This ensures that there are no repeated k-mers, which means the
    number of keys in D is at most (n-k+1)/f. The worst case scenario is if all k-mers in S are
    frequently-occurring, in which case we have a running time of 3(n-k+1)/f with 1 assignment, 1 comparison,
    and 1 addition per iteration, and (n-k+1)/f iterations through the keys in D.

    To create L2, I used a nested for loop. The outer loop iterates through the frequently-occurring
    k-mers stored in L1, with a worst case scenario of having (n-k+1)/f iterations. The inner loop
    always has (n-k+1) iterations as it goes through S, and each iteration compares the current frequent k-mer
    to all the k-mers in S. Instead of creating a new function to compare the values, I use a comparison operator
    as I had investigated the running time for both methods in a Lab, and the operator has a shorter running
    time. If the k-mers match, the index is appended to a sublist. Once it goes through S, the sublist is appended
    to L2. This part of the function has a worst case scenario running time of (1/f)(n-k+1)(c1 + c2(n-k+1)), for
    some constants ci.

    Next, I create another dictionary to assign values to the possible nucleotides (A,C,T,G). I then use this
    dictionary to create all possible point-x mutations of each frequently-occurring k-mer by converting
    the string into a list, which is mutable, then replacing the value at index x with the possible nucleotides. Then,
    I check if each point-x mutation is in the dictionary. If it is, the pair value is counted. If all k-mers in
    S are frequent, then the running time will be c4(n-k+1)/f + c5 for some constants ci.

    Thus the running time of ksearch is
                        C = (a1/f)(n-k+1)^2 + (a2/f + 1)(n-k+1) + a4
    for some ai, with the leading order O((1/f)(n-k+1)^2).
    In the best case scenario, we have 0 frequently-occuring k-mers, where the running time would be b1(n-k+1) + b2,
    for some bi.

    We can see that the the running time always depends on three of the input arguments, namely the sequence S -or more
    specifically, the size of S-, the size of the k-mer, k, and the frequency parameter. The search for the locations
    of the frequent k-mers is the most expensive part of the operation, as it contributes to the leading order term.
    This is because at each iteration through the frequent k-mers, the function has to go through the sequence S.
    """
    n = len(S)
    D = {}
    L1,L2,L3 = [],[],[]

    #create dictionary of all k-mers in S
    D = {S[i:i+k]:S.count(S[i:i+k]) for i in range(n-k+1)}

    #search for frequently-occuring k-mers
    for i in D:
        if D[i] >= f:
            L1.append(i)

    #search for the locations of the frequent k-mers
    for l1 in L1:
        l2 =[]
        for ind in range(0,n-k+1):
            if l1 == S[ind:ind+k]:
                l2.append(ind)
        L2.append(l2)

    #create dictionary of the nucleotides
    base4 = {"A":0,"C":1,"G":2, "T":3}

    #search for the numbers of point-x mutations for the frequent k-mers
    for i in L1:
        count = 0
        l1 = list(i) #convert the k-mer to a mutable list
        mut = [l1.copy() for a in range(4)]
        #create a list of all possible point-x mutations
        for letter in base4:
            mut[base4[letter]][x] = letter
        for j in range(4):
            mut[j] = ''.join(mut[j]) #convert back to a string
            if mut[j] == i: #ignore the original k-mer
                continue
            if mut[j] in D:
                count = count + D[mut[j]]
        L3.append(count)

    return L1,L2,L3



if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGTTCGACGTACTCTTTAG'
    k=2
    x=0
    f=3
    L1,L2,L3=ksearch(S,k,f,x)
